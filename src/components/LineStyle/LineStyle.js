import React from "react";
import './LineStyle.css';

function LineStyle(props) {
    const {variant = 'solid', children, ...rest} = props;
    return (
        <div className={`hline ${variant}`} {...rest} >
            <span>{children}</span>
        </div>
    )
}

export default LineStyle;