import React from "react";
import LineStyle from "./LineStyle";


export default {
    title: 'Line Style',
    component: LineStyle
}

export const Solid = () => <LineStyle variant='solid' > Solid </LineStyle>
export const Dashed = () => <LineStyle variant='dashed' > Dashed </LineStyle>
export const Dotted = () => <LineStyle variant='dotted' > Dotted </LineStyle>
