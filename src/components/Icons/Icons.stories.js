import React from "react";
import Icons from "./Icons";
import * as Icon from 'react-bootstrap-icons';

export default {
    title: 'Icons',
    component: Icons
}

export const CenterThin = () => <Icons variant='center-thin' > <Icon.CircleFill/> </Icons>
export const CenterRegular = () => <Icons variant='center-medium' > <Icon.Apple /> </Icons>
export const StartDotted = () => <Icons variant='start-dotted' > <Icon.Alarm /> </Icons>