import React from "react";
import './Icons.css';

function Icons(props) {
    const {variant = 'center-thin', children, ...rest} = props;
    return (
        <div className={`icons ${variant}`} {...rest} >
            <span>{children}</span>
        </div>
    )
}

export default Icons;