import React from "react";
import './LineWeight.css';

function LineWeight(props) {
    const {variant = 'thin', children, ...rest} = props;
    return (
        <div className={`wline ${variant}`} {...rest} >
            <span>{children}</span>
        </div>
    )
}

export default LineWeight;