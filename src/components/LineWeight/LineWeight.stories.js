import React from "react";
import LineWeight from "./LineWeight";


export default {
    title: 'Line Weight',
    component: LineWeight
}

export const Light = () => <LineWeight variant='thin' >  Light  </LineWeight>
export const Regular = () => <LineWeight variant='medium' >  Regular  </LineWeight>
export const Heavy = () => <LineWeight variant='thick' > Heavy  </LineWeight>