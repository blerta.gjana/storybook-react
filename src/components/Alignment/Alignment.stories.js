import React from "react";
import Alignment from "./Alignment";
import LineStyle from "../LineStyle/LineStyle";


export default {
    title: 'Alignment',
    component: Alignment
}

export const Center = () => <Alignment variant='center'>Center</Alignment>
export const Start = () => <Alignment variant='start'>Start</Alignment>
export const End = () => <Alignment variant='end'>End</Alignment>
