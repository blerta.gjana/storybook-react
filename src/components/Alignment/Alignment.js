import React from "react";
import './Alignment.css';

function Alignment(props) {
    const {variant = 'center', children, ...rest} = props;
    return (
        <div className={`display ${variant}`} {...rest}>
            <span>{children}</span>
        </div>
    )
}

export default Alignment;